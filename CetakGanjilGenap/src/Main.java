import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        System.out.println("Masukkan angka awal");
        int a = sc.nextInt();

        System.out.println("Masukkan angka akhir");
        int b = sc.nextInt();

        sc.close();
        for(int i = a; i<=b; i++){
            if (i % 2 == 0){
                System.out.println("Angka "+i+" adalah genap");
            } else{
                System.out.println("Angka "+i+" adalah ganjil");
            }
        }
    }
}