import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.stream.IntStream;

public class Main {
    public static void main(String[] args) {
        System.out.println("Masukkan operasi");
        Scanner sc = new Scanner(System.in);
        String word = sc.nextLine();
        sc.close();

        System.out.println("output: "+kalkulator(word));
    }

    private static String kalkulator(String input) {
        input = input.toLowerCase();
        String operation = input.replaceAll("\\s","");
        String operators[]=operation.split("[0-9]+");
        String operands[]=operation.split("[/x+-]");

        if (operands.length == 0){
            return "tidak ada operator";
        } else if (operands.length == 1){
            try {
                int a = Integer.parseInt(operands[0]);
                return String.valueOf(a);
            }catch (Exception e){
                return "operands bukan angka";
            }
        } else if(operands.length != operators.length){
            return "input salah";
        } else {
            int firstOperator;
            int secondOperator;
            try {
                firstOperator = Integer.parseInt(operands[0]);
                secondOperator = Integer.parseInt(operands[1]);
            }catch (Exception e){
                return "operands bukan angka";
            }

            String sign = operators[1];

            int result = 0;

            if (sign.equals("+")){
                result = firstOperator + secondOperator;
            } else if (sign.equals("-")){
                result = firstOperator - secondOperator;
            } else if (sign.equals("x")){
                result = firstOperator * secondOperator;
            } else if (sign.equals("/")){
                try{
                    result = firstOperator / secondOperator;
                } catch (Exception e){
                    return "tidak bisa dilakukan";
                }
            }
            return String.valueOf(result);
        }
    }

//    private static String kalkulator(String input) {
//        input = input.toLowerCase();
//        String operation = input.replaceAll("\\s", "");
//
//        String[] parts = operation.split("(?<=[/+x-])|(?=[/+x-])");
//
//        List<Integer> operands = new ArrayList<>();
//        List<String> operators = new ArrayList<>();
//
//        for (String part : parts) {
//            try {
//                int operand = Integer.parseInt(part);
//                operands.add(operand);
//            } catch (NumberFormatException e) {
//                operators.add(part);
//            }
//        }
//
//        if (operands.isEmpty()) {
//            return "tidak ada operator";
//        } else if (operands.size() == 1) {
//            return String.valueOf(operands.get(0));
//        } else if (operators.size() != operands.size() - 1) {
//            return "input salah";
//        } else {
//            int result = operands.get(0);
//            for (int i = 0; i < operators.size(); i++) {
//                int operand = operands.get(i + 1);
//                String operator = operators.get(i);
//                switch (operator) {
//                    case "+":
//                        result += operand;
//                        break;
//                    case "-":
//                        result -= operand;
//                        break;
//                    case "x":
//                        result *= operand;
//                        break;
//                    case "/":
//                        try (IntStream s = IntStream.of(operand)) {
//                            result /= operand;
//                        } catch (ArithmeticException e) {
//                            return "tidak bisa dilakukan";
//                        }
//                        break;
//                    default:
//                        return "input salah";
//                }
//            }
//            return String.valueOf(result);
//        }
//    }
}