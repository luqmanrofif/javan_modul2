import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Masukkan kata");
        String word = sc.nextLine();
        sc.close();

        countVocal(word);
    }

    public static void countVocal(String word) {
        String input = word.toLowerCase();
        boolean isA = false;
        boolean isI = false;
        boolean isU = false;
        boolean isE = false;
        boolean isO = false;

        List<String> listVocal = new ArrayList<>();
        for (int i = 0; i < input.length(); i++) {
            if (!isA && input.charAt(i) == 'a') {
                isA = true;
            } else if (!isI && input.charAt(i) == 'i') {
                isI = true;
            } else if (!isU && input.charAt(i) == 'u') {
                isU = true;
            } else if (!isE && input.charAt(i) == 'e') {
                isE = true;
            } else if (!isO && input.charAt(i) == 'o') {
                isO = true;
            }
        }

        if (isA) {
            listVocal.add("a");
        }

        if (isI) {
            listVocal.add("i");
        }

        if (isU) {
            listVocal.add("u");
        }

        if (isE) {
            listVocal.add("e");
        }

        if (isO) {
            listVocal.add("o");
        }

        if (listVocal.size() == 0) {
            System.out.println("0");
        } else {
            String result = Integer.toString(listVocal.size()) + " yaitu ";
            if (listVocal.size() == 1) {
                result += listVocal.get(0);
            } else if (listVocal.size() == 2) {
                result = result + listVocal.get(0) + " dan " + listVocal.get(1);
            } else {
                for (int j = 0; j < listVocal.size(); j++) {
                    if (j == listVocal.size() - 1) {
                        result = result + "dan " + listVocal.get(j);
                    } else {
                        result = result + listVocal.get(j) + ", ";
                    }

                }
            }
            System.out.println(result);
        }

    }
}